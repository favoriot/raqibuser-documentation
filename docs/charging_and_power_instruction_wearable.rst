Charging & Power On/Off Instruction
===================================

1. Connect the Raqib wearable Raqib wearable to the USB charging 
   cable and start changing.

2. Long press the power on/off button for 3 seconds to turn on 
   the device, if it cannot be turned on, it might due to the 
   low battery volume, please charge your Raqib wearable first 
   and try again.
