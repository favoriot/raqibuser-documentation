Raqib Major Functions
=====================

Contacts
--------

.. image:: images/raqib_major_function_wearable_1.png
   :align: center

The Emergency Contacts added from the App side will be 
automatically sync to the Raqib wearable contact list, you can 
tap on each contact to send SOS alert.

Blood Pressure Monitoring
-------------------------

.. image:: images/raqib_major_function_wearable_2.png
   :align: center

1. Strap the Raqib wearable snugly around your wrist.

2. Put your two fingers on the metal sheet on both sides of the 
   screen.

3. In the BP measurement mode, tap to set your height, weight, 
   gender, age in succession.
   
.. Important:: For people with hypertension, please tap 
   “calibration” to calibrate the device first and then 
   start measurement.

4. Tap “Measure” to start.

5. It will take 25 seconds to finish the measurement.

.. note:: Please keep your two fingers on the metal sheet and 
   your both arms laying comfortably on the desk, and do not 
   move your arms during the BP measurement.


Heart Rate Monitoring
---------------------

.. image:: images/raqib_major_function_wearable_3.png
   :align: center

1. Strap the Raqib wearable snugly around your wrist.

2. Tap the “Heart Rate” icon in Raqib wearable to enter 
   heart rate measurement.

3. Tap “Start” to start the measurement.

ECG Monitoring
--------------

.. image:: images/raqib_major_function_wearable_4.png
   :align: center

1. Ensure that the back of the Raqib wearable metal electrode 
   and skin in good contact.

2. Tap the “ECG” icon in the Raqib wearable Raqib wearable, 
   tap “start” to start the measurement.

3. Place two fingers on the BP/ECG sensors on both metal sides 
   of the screen.

4. After the measurement ended, the ECG data will be displayed at 
   the app side in your cell phone.

.. note:: Cold weather and dry conditions may affect measurement 
   results. Please keep your arms warm and moist.
	 
Pedometer
---------

.. image:: images/raqib_major_function_wearable_5.png
   :align: center

The pedometer is a great tool which will allow you to keep track 
of your steps as well as calories burned, speed and distance.

1. Strap the Raqib wearable snugly around your wrist.

2. Turn on the Raqib wearable and start walking, the device will 
   start to count your walking steps automatically.
 
Settings
--------

.. image:: images/raqib_major_function_wearable_6.png
   :align: center

1. In “Settings → Information”, you can check and set your 
   personal information, the personal information you set on the 
   app side will be automatically sync to your Raqib wearable 
   Raqib wearable device.

2. In “Settings → wakeup LCD, you can set the LCD status. If the 
   LCD is set to “on”, the LCD display will wake up automatically 
   from sleep mode when you rise your hands.

3. In “Setting → Raqib wearable bind”, you can scan the QR code 
   to download the Raqib Mobile app.
