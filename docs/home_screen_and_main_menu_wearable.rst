Home Screen and Main Menu
=========================

The Raqib wearable home screen can display date and current time.

.. image:: images/home_screen_and_main_menu_wearable_1.png
   :align: center