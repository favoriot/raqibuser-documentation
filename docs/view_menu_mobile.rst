View Menu
=========

1. Tap the Hamburger icon.

.. image:: images/view_menu_mobile_1.png
   :align: center

2. List of menu displayed.

.. image:: images/view_menu_mobile_2.png
   :align: center