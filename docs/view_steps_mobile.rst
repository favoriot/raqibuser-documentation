View Steps
==========

1. Tap ‘Steps’.

.. image:: images/view_steps_mobile_1.png
   :align: center

2. View pilgrim’s steps reading here.

.. image:: images/view_steps_mobile_2.png
   :align: center