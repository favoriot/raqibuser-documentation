Flexi-1 and Button Overview
===========================

.. image:: images/flexi-1_and_button_overview_wearable_1.png
   :align: center

1. SOS button

2. Sleep/wake button

.. note:: Press to wake your device or put it to sleep. Press and hold 
   the button to turn your device off or on.

3. Left blood pressure/ECG sensor

4. Right blood pressure/ECG sensor

.. note:: When taking blood pressure/ECG measurement, ensure 
   your fingers to hold the sides of the sensors to start the 
   measurement.