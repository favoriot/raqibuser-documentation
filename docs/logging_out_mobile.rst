Logging Out
===========

1. Tap ‘Log Out’.

.. image:: images/logging_out_mobile_1.png
   :align: center

2. Confirm log out.

.. image:: images/logging_out_mobile_2.png
   :align: center